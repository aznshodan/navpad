function [ frontier ] = get_frontier( grid_map_struct, grid_params, distance_map_struct, wavefront_params)
%GET_FRONTIER Summary of this function goes here
%   Detailed explanation goes here
data = grid_map_struct.data;

% filter the data by using inverse sensor model
% if greater than log_odds_occ then data is 1 otherwise 0
data(data>=grid_params.log_odds_occ) = 1;
data(data<grid_params.log_odds_occ) = 0;

% filter it out cleanly??
data = edge(data);
% find cells that are occupied
ind = find(data>0);

% filter out all the obstacles that are less than the min obstacle distance
ind(distance_map_struct.data(ind)<=wavefront_params.min_obstacle_distance)=[];

% filter out all the obstacles that are greater than max obstacle distance
if (strcmp(wavefront_params.mode ,'obstacle'))
    ind(distance_map_struct.data(ind)>=wavefront_params.max_obstacle_distance)=[];
end

% set data to 0
data(:)=0;

% set data at ind to 1
data(ind)=1;

% get all the rows and cols 
[r, c] = ind2sub(size(grid_map_struct.data),ind);

% add it to frontier
[frontier(:,1),frontier(:,2)] = index2coord([r c],grid_map_struct.scale,grid_map_struct.min(1),grid_map_struct.min(2));
end

