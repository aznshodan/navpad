function displayPathsById(pathIds,globalPathList,globalPathIds,c)
%DISPLAYMAPVELOCITYPATHS Summary of this function goes here
%   Detailed explanation goes here
for i=1:size(pathIds,1)
    ids = globalPathIds(pathIds(i),:);
    pathxy = globalPathList(ids(1):ids(2),2:3);
    hold on
    plot(pathxy(:,1),pathxy(:,2),c,'LineWidth',2)    
end

end