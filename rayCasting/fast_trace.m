function [x,y,r,collision] = fast_trace(map_struct,angle_range, angle_resolution, max_range, range_resolution, position, yaw)
%% fast ray tracing in 2d -- can be made faster but sticking with this for
%% now
% inputs angle Range, angle Resolution, position vector
% outputs?
% x ? 
% y ? 
% r ?
% collision ? 
angles = angle_range(1):angle_resolution:angle_range(2);
x = zeros(size(position,1),size(angles,2));
y = x;
r = x;
collision = x;
ranges = [0:range_resolution:max_range]';

%%%%%%%%%%%%%%%%%%%
%%Display Values %%
%%%%%%%%%%%%%%%%%%%
%disp('%%%%%%%%%%%%%%%%%%%%%%%');
%disp('%% FAST TRACE INPUTS %%');
%disp('%%%%%%%%%%%%%%%%%%%%%%%');
%disp('INPUTS');
%disp('angle range is: ');
%disp(angle_range);
%disp('angle resolution is: ');
%disp(angle_resolution);
%disp('max range is: ');
%disp(max_range);
%disp('range_resolution is: ');
%disp(range_resolution);
%disp('position is: ');
%disp(position);
%disp('yaw is: ');
%disp(yaw);
%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%

for j=1:size(position,1)
    %add yaw to all angles (offseting?)
    %Question: why add yaw to angles ? 
    angles2 = bsxfun(@plus, angles,yaw(j));
    %for every dθ (from -3.14 to 3.14) 
    for i=1:size(angles,2)  
        
        %generates a matrix of [(r0cosθ, r0sinθ), (r1cosθ, r1sinθ), ...]
        %where r0 < r1 < r2 < r3... < rn
        xy = bsxfun(@plus,position(j,1:2),bsxfun(@times,ranges,[cos(angles2(i)),sin(angles2(i))]));
        %convert them into xy coordinates on the map (via rounding, etc)
        %array of all the possibe coordinates from current position
        xy = coord2index(xy(:,1),xy(:,2),map_struct.scale,map_struct.min(1),map_struct.min(2)); 
        
        %checking out of bounds
        %find the first set of coordinates that is out of bounds
        [outId1,~] = find(xy(:,1)<1|xy(:,2)<1,1,'first'); %outId1 is range r where it first goes OOB
        %find the first set of coordinates that is out of bounds
        %outId2 is the range r where it first goes OOB
        [outId2,~] = find(xy(:,1)>=size(map_struct.data,1)|xy(:,2)>=size(map_struct.data,2),1,'first');
        
        
        
        %get the min xcoord that is within bounds for all range
        %check x coordinates
        xy(:,1) = min(size(map_struct.data, 1)*(ones(size(xy(:,1)))), max((ones(size(xy(:,1)))), xy(:,1)));
        %get the ycoord that is within bounds for all range
        %check y coordinates
        xy(:,2) = min(size(map_struct.data, 2)*(ones(size(xy(:,2)))), max((ones(size(xy(:,2)))), xy(:,2)));
        
        
        
        %get the indexes of desired cells on the map
        ind = sub2ind(size(map_struct.data),xy(:,1),xy(:,2));
        %find the first cell that is less than 0.5. (0.5?)
        [collisionId,~] = find(map_struct.data(ind)<0.5,1,'first');
        
        %find the min
        collisionId = min([collisionId;outId1;outId2]);
        %if collisionId is empty, then take the furthest cell
        if isempty(collisionId) 
            reflection_range = max_range;
            collision_det = 0;
            xy = xy(end,:); %this is the coordinates of the furthest cell
        %if there is collisionId, take the collisionId as the furthest
        else
            collisionId = max(collisionId-1,1); %subtract 1 to be within bounds
            reflection_range = ranges(collisionId);
            collision_det = 1;            
            xy = xy(collisionId,:); %coordinates of the cell before collision
            [xy(:,1), xy(:,2)] = index2coord([xy(:,1) xy(:,2)],map_struct.scale,map_struct.min(1),map_struct.min(2));
        end
        
        x(j,i) = xy(1);
        y(j,i) = xy(2);
        r(j,i) = reflection_range;
        collision(j,i) = collision_det;
    end
end

%%%%%%%%%%%%%%%%%%%
%%Display Values %%
%%%%%%%%%%%%%%%%%%%
%disp('%%%%%%%%%%%%%%%%%%%%%%%');
%disp('%%FAST TRACE OUTPUTS%%%');
%disp('%%%%%%%%%%%%%%%%%%%%%%%');
%disp('x: ');
%disp(x);
%disp('y: ');
%disp(y);
%disp('r: ');
%disp(r);
%disp('collision: ');
%disp(collision);
%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%
