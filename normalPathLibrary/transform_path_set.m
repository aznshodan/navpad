function [ tf_path_set ] = transform_path_set(  state, path_set )
%TRANSFORM_PATH_SET Summary of this function goes here
%   Detailed explanation goes here
tf_path_set = [];
for path = path_set
    tf_path = path;
    tf_path.x = state.x + path.x*cos(state.psi) - path.y*sin(state.psi);
    tf_path.y = state.y + path.x*sin(state.psi) + path.y*cos(state.psi);
    tf_path.psi = path.psi + state.psi;
    tf_path_set = [tf_path_set tf_path];
end

end

