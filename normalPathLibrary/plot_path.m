function handle = plot_path( path, handle )
%PLOT_PATH Summary of this function goes here
%   Detailed explanation goes here

if (nargin < 2)
    handle = plot(path.x, path.y, 'r');
    axis equal;
else
    set(handle, 'XData', path.x, 'YData', path.y); 
end

end

