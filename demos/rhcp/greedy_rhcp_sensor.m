%% Example: Greedily select paths without safety considerations
clc
clear 
close all

%% Add required paths
addpath(genpath('../../fastMarching'));
addpath(genpath('../../maps'));
addpath(genpath('../../normalPathLibrary'));
addpath(genpath('../../gridmapping'));
addpath(genpath('../../rayCasting'));
addpath(genpath('utils'));

%% Get parameters
grid_params = get_grid_params(1);
laser_params = get_laser_params(1);
planner_params = get_planner_params(1);

%% Get world map and robot's belief map
[ world_map, robo_map ] = get_map_obj( '../../maps/example_map.tif', grid_params );

%% Initialize problem statement
problem.start.x = 350; problem.start.y = 500; problem.start.psi = 0;
problem.goal.x = 238; problem.goal.y = 344; problem.goal_tol = 10; 

%% Get display object
display_obj = get_display_obj( problem, world_map, planner_params, false );

%% Main loop
state = problem.start;
history = state;
display_obj.handle_set.quiv_f = quiver(state.x, state.y, 0, 0);
while (~has_reached_goal(state, problem))
    % Update belief map
    robo_map = update_belief( robo_map, state, world_map, laser_params, grid_params, display_obj );
    
    % Plan 
    [chosen_path, valid_plan] = select_plan( state, problem, robo_map, planner_params, display_obj );
    
    if (~valid_plan)
        display('no plan found');
        break;
    end
    
    % Call dynamics
    [state, history] = forward_sim_trajectory( state, history, chosen_path, planner_params, display_obj );
    
    pause(0.1);
end

clean_display( display_obj );